<?php

declare(strict_types=1);

namespace Workbench\App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use mrzainulabideen\AESEncrypt\Database\Eloquent\ModelEncrypt;
use Override;
use Workbench\Database\Factories\UserFactory as UserFactoryAlias;

class User extends ModelEncrypt
{
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillableEncrypt = [
        'name',
    ];

    protected static function newFactory(): UserFactoryAlias
    {
        return UserFactoryAlias::new();
    }
    /**
     * The attributes that should be cast.
     */
    #[Override]
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }
}
