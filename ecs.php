<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ArrayNotation\NoMultilineWhitespaceAroundDoubleArrowFixer;
use PhpCsFixer\Fixer\ArrayNotation\NormalizeIndexBraceFixer;
use PhpCsFixer\Fixer\ArrayNotation\WhitespaceAfterCommaInArrayFixer;
use PhpCsFixer\Fixer\AttributeNotation\OrderedAttributesFixer;
use PhpCsFixer\Fixer\ClassNotation\ClassAttributesSeparationFixer;
use PhpCsFixer\Fixer\ClassNotation\OrderedTraitsFixer;
use PhpCsFixer\Fixer\ClassNotation\OrderedTypesFixer;
use PhpCsFixer\Fixer\Import\NoUnusedImportsFixer;
use PhpCsFixer\Fixer\Operator\NoSpaceAroundDoubleColonFixer;
use PhpCsFixer\Fixer\Operator\NotOperatorWithSuccessorSpaceFixer;
use PhpCsFixer\Fixer\Operator\ObjectOperatorWithoutWhitespaceFixer;
use PhpCsFixer\Fixer\Operator\UnaryOperatorSpacesFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocLineSpanFixer;
use PhpCsFixer\Fixer\ReturnNotation\SimplifiedNullReturnFixer;
use PhpCsFixer\Fixer\Semicolon\MultilineWhitespaceBeforeSemicolonsFixer;
use PhpCsFixer\Fixer\Strict\DeclareStrictTypesFixer;
use PhpCsFixer\Fixer\Strict\StrictComparisonFixer;
use PhpCsFixer\Fixer\Whitespace\ArrayIndentationFixer;
use PhpCsFixer\Fixer\Whitespace\BlankLineBeforeStatementFixer as BlankLineBeforeStatementFixerAlias;
use PhpCsFixer\Fixer\Whitespace\MethodChainingIndentationFixer;
use PhpCsFixer\Fixer\Whitespace\NoExtraBlankLinesFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayOpenerAndCloserNewlineFixer;
use Symplify\CodingStandard\Fixer\LineLength\LineLengthFixer;
use Symplify\CodingStandard\Fixer\Spacing\MethodChainingNewlineFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return ECSConfig::configure()
    ->withPaths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
        __DIR__ . '/workbench',
        __DIR__ . '/ecs.php',
        __DIR__ . '/rector.php',
    ])
    ->withRules(rules: [
        BlankLineBeforeStatementFixerAlias::class,
        NoMultilineWhitespaceAroundDoubleArrowFixer::class,
        NoExtraBlankLinesFixer::class,
        ArrayIndentationFixer::class,
        NormalizeIndexBraceFixer::class,
        NoUnusedImportsFixer::class,
        WhitespaceAfterCommaInArrayFixer::class,
        DeclareStrictTypesFixer::class,
        NoSpaceAroundDoubleColonFixer::class,
        ObjectOperatorWithoutWhitespaceFixer::class,
        SimplifiedNullReturnFixer::class,
        MultilineWhitespaceBeforeSemicolonsFixer::class,
        OrderedTraitsFixer::class,
        StrictComparisonFixer::class,
        OrderedAttributesFixer::class,
        OrderedTypesFixer::class,
    ])
    ->withConfiguredRule(UnaryOperatorSpacesFixer::class, ['only_dec_inc' => false])
    ->withPreparedSets(
        psr12: true,
        symplify: true,
        arrays: true,
        comments: true,
        docblocks: true,
        spaces: true,
        namespaces: true,
        controlStructures: true,
        phpunit: true,
    )
    ->withSkip([
        LineLengthFixer::class => null,
        MethodChainingIndentationFixer::class => null,
        MethodChainingNewlineFixer::class => null,
        ArrayOpenerAndCloserNewlineFixer::class => null,
        ClassAttributesSeparationFixer::class => null,
        PhpdocLineSpanFixer::class => null,
        NotOperatorWithSuccessorSpaceFixer::class => null,
        MultilineWhitespaceBeforeSemicolonsFixer::class => null,
    ]);
