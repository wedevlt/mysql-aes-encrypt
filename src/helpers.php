<?php

declare(strict_types=1);

namespace AESEncrypt;

if (!function_exists('AESEncrypt\mysqldecrypt')) {
    /**
     * Creates a Stringy object and returns it on success.
     */
    function mysqldecrypt($column, $alias = null): string
    {
        return sprintf("AES_DECRYPT(SUBSTRING_INDEX(%s, '.iv.', 1), '", $column) . config('aesEncrypt.key') . sprintf("', SUBSTRING_INDEX(%s, '.iv.', -1)) as `", $column) . ($alias ?: $column) . '`';
    }
}

if (!function_exists('AESEncrypt\mysqlencrypt')) {
    /**
     * Creates a Stringy object and returns it on success.
     */
    function mysqlencrypt($column, $alias = null): string
    {
        $iv = bin2hex(random_bytes(16));

        return sprintf("CONCAT(AES_ENCRYPT(%s, '", $column) . config('aesEncrypt.key') . sprintf("', '%s'), '.iv.','%s') as `", $iv, $iv) . ($alias ?: $column) . '`';
    }
}
