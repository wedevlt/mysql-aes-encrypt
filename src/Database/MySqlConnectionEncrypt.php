<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database;

use Illuminate\Database\MySqlConnection;
use mrzainulabideen\AESEncrypt\Database\Query\Grammars\MySqlGrammarEncrypt as QueryGrammar;
use Override;

class MySqlConnectionEncrypt extends MySqlConnection
{
    /**
     * Get the default query grammar instance.
     */
    #[Override]
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new QueryGrammar());
    }
}
