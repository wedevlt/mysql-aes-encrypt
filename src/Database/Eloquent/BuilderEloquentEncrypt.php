<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use mrzainulabideen\AESEncrypt\Database\Query\BuilderEncrypt as QueryBuilder;
use Override;

class BuilderEloquentEncrypt extends Builder
{
    /**
     * Create a new Eloquent query builder instance.
     */
    public function __construct(QueryBuilder $query)
    {
        $this->query = $query;
    }

    /**
     * Set a model instance for the model being queried.
     */
    #[Override]
    public function setModel(Model $model)
    {
        if (env('APP_ENV') === 'testing') {
            parent::setModel($model);

            return $this;
        }

        $this->model = $model;

        $this->query->from($model->getTable());

        $this->query->setFillableEncrypt($model->getfillableEncrypt());

        return $this;
    }
}
