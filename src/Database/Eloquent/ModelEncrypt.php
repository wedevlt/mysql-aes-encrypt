<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use mrzainulabideen\AESEncrypt\Database\Query\BuilderEncrypt as QueryBuilder;
use Override;

abstract class ModelEncrypt extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillableEncrypt = [];

    /**
     * Create a new Eloquent model instance.
     */
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();

        $this->initializeTraits();

        $this->syncOriginal();

        $this->fill($attributes);

        $this->setSessionVariables();
    }

    public function setSessionVariables(): void
    {
        if (DB::getDriverName() !== 'mysql') {
            return;
        }

        $aesKeyResult = DB::select('SELECT @AESKEY as aesKey');

        if (!isset($aesKeyResult[0]) || !$aesKeyResult[0]?->aesKey) {
            $key = config('aesEncrypt.key');
            $mode = config('aesEncrypt.mode');
            DB::statement(sprintf("SET @@SESSION.block_encryption_mode = '%s'", $mode));
            DB::statement(sprintf("SET @AESKEY = '%s'", $key));
        }
    }

    /**
     * Get a new query builder that doesn't have any global scopes.
     */
    #[Override]
    public function newQueryWithoutScopes()
    {
        $builder = $this->newEloquentBuilder($this->newBaseQueryBuilder());

        // Once we have the query builders, we will set the model instances so the
        // builder can easily access any information it may need from the model
        // while it is constructing and executing various queries against it.

        return $builder->setModel($this)
                    //->setfillableColumns($this->fillable)
                    ->with($this->with)
                    ->withCount($this->withCount);
    }

    /**
     * Create a new Eloquent query builder for the model.
     */
    #[Override]
    public function newEloquentBuilder($query)
    {
        return new BuilderEloquentEncrypt($query);
    }

    // /**
    //  * Get the table associated with the model.
    //  *
    //  * @return string
    //  */
    public function getfillableEncrypt()
    {
        return $this->fillableEncrypt;
    }

    /**
     * Get a new query builder instance for the connection.
     */
    #[Override]
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        return new QueryBuilder(
            $connection,
            $connection->getQueryGrammar(),
            $connection->getPostProcessor()
        );
    }
}
