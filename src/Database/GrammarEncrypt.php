<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\JoinClause;
use mrzainulabideen\AESEncrypt\Database\Query\BuilderEncrypt;
use Override;

class GrammarEncrypt extends Grammar
{
    /**
     * Compile a select query into SQL.
     */
    #[Override]
    public function compileSelect(Builder $query)
    {
        // If the query does not have any columns set, we'll set the columns to the
        // * character to just get all of the columns from the database. Then we
        // can build the query and concatenate all the pieces together as one.
        $original = $query->columns;
        // $columnsEncrypt = $this->columnsEncrypt;

        if ($query->columns === null) {
            $query->columns = ['*'];
        }

        // To compile the query, we'll spin through each component of the query and
        // see if that component exists. If it does we'll just call the compiler
        // function for the component which is responsible for making the SQL.
        $sql = trim(
            $this->concatenate(
                $this->compileComponents($query)
            )
        );

        $query->columns = $original;

        return $sql;
    }

    /**
     * Compile the "where" portions of the query.
     */
    #[Override]
    public function compileWheres(Builder $query)
    {
        // Each type of where clauses has its own compiler function which is responsible
        // for actually creating the where clauses SQL. This helps keep the code nice
        // and maintainable since each clause has a very small method that it uses.
        if ($query->wheres === null) {
            return '';
        }

        // If we actually have some where clauses, we will strip off the first boolean
        // operator, which is added by the query builders for convenience so we can
        // avoid checking for the first clauses in each of the compilers methods.
        if (count($sql = $this->compileWheresToArray($query)) > 0) {
            return $this->concatenateWhereClauses($query, $sql);
        }

        return '';
    }

    /**
     * Compile an exists statement into SQL.
     */
    #[Override]
    public function compileExists(Builder $query)
    {
        $select = $this->compileSelect($query);

        return sprintf('select exists(%s) as %s', $select, $this->wrap('exists'));
    }

    /**
     * Compile an insert statement into SQL.
     */
    #[Override]
    public function compileInsert(Builder $query, array $values)
    {
        $this->columnsEncrypt = [];
        if ($query instanceof BuilderEncrypt) {
            $instance = 'BuilderEncrypt';
            $this->columnsEncrypt = $query->getfillableEncrypt();
        }

        // Essentially we will force every insert to be treated as a batch insert which
        // simply makes creating the SQL easier for us since we can utilize the same
        // basic routine regardless of an amount of records given to us to insert.
        $table = $this->wrapTable($query->from);

        if (!is_array(reset($values))) {
            $values = [$values];
        }

        $columns = $this->columnize(array_keys(reset($values)));

        // We need to build a list of parameter place-holders of values that are bound
        // to the query. Each insert should have the exact same amount of parameter
        // bindings so we will loop through the record and parameterize them all.
        $parameters = collect($values)->map(fn ($record): string => '(' . $this->parameterize($record, $this->columnsEncrypt) . ')')->implode(', ');

        return sprintf('insert into %s (%s) values %s', $table, $columns, $parameters);
    }

    /**
     * Compile an insert and get ID statement into SQL.
     */
    #[Override]
    public function compileInsertGetId(Builder $query, $values, $sequence)
    {
        return $this->compileInsert($query, $values);
    }

    // **********************
    // Illuminate Grammar
    // **********************
    /**
     * Convert an array of column names into a delimited string.
     */
    #[Override]
    public function columnize(array $columns, array $columnsEncrypt = [], $forceAlias = false)
    {
        $columnize = [];

        $columns = $this->addColumnsToWildcard($columns, $columnsEncrypt);

        foreach ($columns as $value) {
            $value = $this->wrap($value, false, $columnsEncrypt);
            if ($forceAlias
                && !empty($columnsEncrypt)
                && !str_contains(strtolower($value), ' as ')
                && !str_contains(strtolower($value), '*')) {
                preg_match("/\`.*?\`/", $value, $alias);
                $value = $value . ' as ' . $alias[0];
            }

            $columnize[] = $value;
        }

        return implode(', ', $columnize);
    }

    /**
     * if columns only contain a wildcard we add the encrypted columns to decrypt
     */
    public function addColumnsToWildcard(array $columns, array $columnsEncrypt)
    {
        if (!empty($columns) && is_string($columns[0]) && str_contains(strtolower($columns[0]), '*')) {
            $columns = array_merge($columns, $columnsEncrypt);
        }

        return $columns;
    }

    /**
     * Wrap a value in keyword identifiers.
     */
    #[Override]
    public function wrap($value, $prefixAlias = false, $columnsEncrypt = [])
    {
        if ($this->isExpression($value)) {
            return $this->getValue($value);
        }

        // If the value being wrapped has a column alias we will need to separate out
        // the pieces so we can wrap each of the segments of the expression on it
        // own, and then joins them both back together with the "as" connector.
        if (str_contains(strtolower($value), ' as ')) {
            return $this->wrapAliasedValue($value, $prefixAlias, $columnsEncrypt);
        }

        return $this->wrapSegments(explode('.', $value), $columnsEncrypt);
    }

    /**
     * Create query parameter place-holders for an array.
     */
    #[Override]
    public function parameterize(array $values, $columnsEncrypt = [])
    {
        $parametize = [];
        foreach ($values as $key => $value) {
            $valueParameter = $this->parameter($value);
            if ($key && in_array($key, $columnsEncrypt)) {
                $valueParameter = $this->wrapValueEncrypt($valueParameter);
            }

            $parametize[] = $valueParameter;
        }

        return implode(', ', $parametize);
    }

    /**
     * Get the appropriate query parameter place-holder for a value.
     */
    #[Override]
    public function parameter($value)
    {
        return $this->isExpression($value) ? $this->getValue($value) : '?';
    }

    /**
     * Compile the components necessary for a select clause.
     */
    #[Override]
    protected function compileComponents(Builder $query)
    {
        $sql = [];

        foreach ($this->selectComponents as $component) {
            // To compile the query, we'll spin through each component of the query and
            // see if that component exists. If it does we'll just call the compiler
            // function for the component which is responsible for making the SQL.
            if ($query->{$component} !== null) {
                $method = 'compile' . ucfirst($component);

                $sql[$component] = $this->{$method}($query, $query->{$component});
            }
        }

        return $sql;
    }

    /**
     * Compile the "select *" portion of the query.
     */
    #[Override]
    protected function compileColumns(Builder $query, $columns)
    {
        // If the query is actually performing an aggregating select, we will let that
        // compiler handle the building of the select clauses, as it will need some
        // more syntax that is best handled by that function to keep things neat.
        if ($query->aggregate !== null) {
            return;
        }

        $select = $query->distinct ? 'select distinct ' : 'select ';

        return $select . $this->columnize($columns, $this->columnsEncrypt, true);
    }

    /**
     * Get an array of all the where clauses for the query.
     */
    #[Override]
    protected function compileWheresToArray($query)
    {
        return collect($query->wheres)->map(fn ($where): string => $where['boolean'] . ' ' . $this->{'where' . $where['type']}($query, $where))->all();
    }

    /**
     * Compile a basic where clause.
     */
    #[Override]
    protected function whereBasic(Builder $query, $where)
    {
        $value = $this->parameter($where['value']);

        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' ' . $where['operator'] . ' ' . $value;
    }

    /**
     * Compile a "where in" clause.
     */
    #[Override]
    protected function whereIn(Builder $query, $where)
    {
        if (!empty($where['values'])) {
            return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' in (' . $this->parameterize($where['values']) . ')';
        }

        return '0 = 1';
    }

    /**
     * Compile a "where not in" clause.
     */
    #[Override]
    protected function whereNotIn(Builder $query, $where)
    {
        if (!empty($where['values'])) {
            return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' not in (' . $this->parameterize($where['values']) . ')';
        }

        return '1 = 1';
    }

    /**
     * Compile a where in sub-select clause.
     */
    protected function whereInSub(Builder $query, array $where): string
    {
        return $this->wrap($where['column']) . ' in (' . $this->compileSelect($where['query']) . ')';
    }

    /**
     * Compile a where not in sub-select clause.
     */
    protected function whereNotInSub(Builder $query, array $where): string
    {
        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' not in (' . $this->compileSelect($where['query']) . ')';
    }

    /**
     * Compile a "where null" clause.
     */
    #[Override]
    protected function whereNull(Builder $query, $where)
    {
        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' is null';
    }

    /**
     * Compile a "where not null" clause.
     */
    #[Override]
    protected function whereNotNull(Builder $query, $where)
    {
        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' is not null';
    }

    /**
     * Compile a "between" where clause.
     */
    #[Override]
    protected function whereBetween(Builder $query, $where)
    {
        $between = $where['not'] ? 'not between' : 'between';

        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' ' . $between . ' ? and ?';
    }

    /**
     * Compile a "where date" clause.
     */
    #[Override]
    protected function whereDate(Builder $query, $where)
    {
        return $this->dateBasedWhere('date', $query, $where);
    }

    /**
     * Compile a "where time" clause.
     */
    #[Override]
    protected function whereTime(Builder $query, $where)
    {
        return $this->dateBasedWhere('time', $query, $where);
    }

    /**
     * Compile a "where day" clause.
     */
    #[Override]
    protected function whereDay(Builder $query, $where)
    {
        return $this->dateBasedWhere('day', $query, $where);
    }

    /**
     * Compile a "where month" clause.
     */
    #[Override]
    protected function whereMonth(Builder $query, $where)
    {
        return $this->dateBasedWhere('month', $query, $where);
    }

    /**
     * Compile a "where year" clause.
     */
    #[Override]
    protected function whereYear(Builder $query, $where)
    {
        return $this->dateBasedWhere('year', $query, $where);
    }

    /**
     * Compile a date based where clause.
     */
    #[Override]
    protected function dateBasedWhere($type, Builder $query, $where)
    {
        $value = $this->parameter($where['value']);

        return $type . '(' . $this->wrap($where['column'], false, $this->columnsEncrypt) . ') ' . $where['operator'] . ' ' . $value;
    }

    /**
     * Compile a where clause comparing two columns..
     */
    #[Override]
    protected function whereColumn(Builder $query, $where)
    {
        return $this->wrap($where['first'], false, $this->columnsEncrypt) . ' ' . $where['operator'] . ' ' . $this->wrap($where['second']);
    }

    /**
     * Compile a nested where clause.
     */
    #[Override]
    protected function whereNested(Builder $query, $where)
    {
        // Here we will calculate what portion of the string we need to remove. If this
        // is a join clause query, we need to remove the "on" portion of the SQL and
        // if it is a normal query we need to take the leading "where" of queries.
        $offset = $query instanceof JoinClause ? 3 : 6;

        return '(' . substr($this->compileWheres($where['query']), $offset) . ')';
    }

    /**
     * Compile a where condition with a sub-select.
     */
    #[Override]
    protected function whereSub(Builder $query, $where)
    {
        $select = $this->compileSelect($where['query']);

        return $this->wrap($where['column'], false, $this->columnsEncrypt) . ' ' . $where['operator'] . sprintf(' (%s)', $select);
    }

    /**
     * Compile a where exists clause.
     */
    #[Override]
    protected function whereExists(Builder $query, $where)
    {
        return 'exists (' . $this->compileSelect($where['query']) . ')';
    }

    /**
     * Compile a where exists clause.
     */
    #[Override]
    protected function whereNotExists(Builder $query, $where)
    {
        return 'not exists (' . $this->compileSelect($where['query']) . ')';
    }

    /**
     * Compile the "group by" portions of the query.
     */
    #[Override]
    protected function compileGroups(Builder $query, $groups)
    {
        return 'group by ' . $this->columnize($groups, $this->columnsEncrypt);
    }

    /**
     * Compile a basic having clause.
     */
    #[Override]
    protected function compileBasicHaving($having)
    {
        // TODO: Need check
        // $column = $this->wrap($having['column']);
        $column = $this->wrap($having['column'], false, $this->columnsEncrypt);

        $parameter = $this->parameter($having['value']);

        return $having['boolean'] . ' ' . $column . ' ' . $having['operator'] . ' ' . $parameter;
    }

    /**
     * Compile the query orders to an array.
     */
    #[Override]
    protected function compileOrdersToArray(Builder $query, $orders)
    {
        // TODO: Need check
        return array_map(fn ($order) => !isset($order['sql'])
                    ? $this->wrap($order['column'], false, $this->columnsEncrypt) . ' ' . $order['direction']
                    : $order['sql'], $orders);
    }

    /**
     * Wrap a value that has an alias.
     */
    #[Override]
    protected function wrapAliasedValue($value, $prefixAlias = false, $columnsEncrypt = [])
    {
        $segments = preg_split('/\s+as\s+/i', $value);

        // If we are wrapping a table we need to prefix the alias with the table prefix
        // as well in order to generate proper syntax. If this is a column of course
        // no prefix is necessary. The condition will be true when from wrapTable.
        if ($prefixAlias) {
            $segments[1] = $this->tablePrefix . $segments[1];
        }

        return $this->wrap($segments[0], false, $columnsEncrypt) . ' as ' . $this->wrapValue(
            $segments[1]
        );
    }

    /**
     * Wrap the given value segments.
     */
    #[Override]
    protected function wrapSegments($segments, $columnsEncrypt = [])
    {
        return collect($segments)->map(function ($segment, $key) use ($segments, $columnsEncrypt) {
            return $key === 0 && count($segments) > 1
                            ? $this->wrapTable($segment)
                            : $this->wrapValue($segment);
            // Colocar aqui encrypt
        })->implode('.');
    }
}
