<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Connectors;

use Illuminate\Database\Connection;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Database\SqlServerConnection;
use InvalidArgumentException;
use mrzainulabideen\AESEncrypt\Database\MySqlConnectionEncrypt;
use mrzainulabideen\AESEncrypt\Database\PsqlConnectionEncrypt;
use Override;

class ConnectionFactoryEncrypt extends ConnectionFactory
{
    /**
     * Create a new connection instance.
     */
    #[Override]
    protected function createConnection($driver, $connection, $database, $prefix = '', array $config = [])
    {
        if ($resolver = Connection::getResolver($driver)) {
            return $resolver($connection, $database, $prefix, $config);
        }

        return match ($driver) {
            'mysql' => new MySqlConnectionEncrypt($connection, $database, $prefix, $config),
            'pgsql' => new PsqlConnectionEncrypt($connection, $database, $prefix, $config),
            'sqlite' => new SQLiteConnection($connection, $database, $prefix, $config),
            'sqlsrv' => new SqlServerConnection($connection, $database, $prefix, $config),
            default => throw new InvalidArgumentException(sprintf('Unsupported driver [%s]', $driver)),
        };
    }
}
