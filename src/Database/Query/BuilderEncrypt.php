<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Query;

use Illuminate\Database\Query\Builder as BuilderCore;
use Override;

class BuilderEncrypt extends BuilderCore
{
    protected $fillableEncrypt;

    protected $fillableColumns;

    /**
     * Execute the query as a "select" statement.
     */
    #[Override]
    public function get($columns = ['*'])
    {
        if ($columns = ['*']) {
            $columns = $this->fillableColumns;
        }

        $original = $this->columns;

        if ($original === null) {
            $this->columns = $columns;
        }

        $results = $this->processor->processSelect($this, $this->runSelect());

        $this->columns = $original;

        return collect($results);
    }

    /**
     * Set a model instance for the model being queried.
     */
    public function setfillableEncrypt($fillableEncrypt): static
    {
        $this->fillableEncrypt = $fillableEncrypt;

        return $this;
    }

    /**
     * Set a model instance for the model being queried.
     */
    public function getfillableEncrypt()
    {
        return !empty($this->fillableEncrypt) ? $this->fillableEncrypt : [];
    }

    /**
     * Set a fillable columns
     */
    public function setfillableColumns($fillableColumns): static
    {
        $this->fillableColumns = $fillableColumns;

        return $this;
    }

    /**
     * Return fillable columns
     */
    public function getfillableColumns()
    {
        return !empty($this->fillableColumns) ? $this->fillableColumns : [];
    }
}
