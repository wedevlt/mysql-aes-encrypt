<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Query\Grammars;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JsonExpression;
use Illuminate\Support\Str;
use InvalidArgumentException;
use mrzainulabideen\AESEncrypt\Database\GrammarEncrypt;
use mrzainulabideen\AESEncrypt\Database\Query\BuilderEncrypt;
use Override;

class MySqlGrammarEncrypt extends GrammarEncrypt
{
    protected $AESENCRYPT_KEY;
    protected $AESENCRYPT_MODE;

    protected $columnsEncrypt = [];

    /**
     * The components that make up a select clause.
     *
     * @var array
     */
    protected $selectComponents = [
        'aggregate',
        'columns',
        'from',
        'joins',
        'wheres',
        'groups',
        'havings',
        'orders',
        'limit',
        'offset',
        'lock',
    ];

    public function __construct()
    {
        $this->AESENCRYPT_KEY = config('aesEncrypt.key');
        $this->AESENCRYPT_MODE = config('aesEncrypt.mode');

        if (empty($this->AESENCRYPT_KEY)) {
            throw new InvalidArgumentException('Set encryption key in .env file, use this alias APP_AESENCRYPT_KEY');
        }

        if (empty($this->AESENCRYPT_MODE)) {
            throw new InvalidArgumentException('Set encryption mode in .env file, use this alias APP_AESENCRYPT_MODE, we recommend using the default: aes-256-cbc');
        }
    }

    /**
     * Compile a select query into SQL.
     */
    #[Override]
    public function compileSelect(Builder $query)
    {
        $this->columnsEncrypt = [];

        if ($query instanceof BuilderEncrypt) {
            $instance = 'BuilderEncrypt';
            $this->columnsEncrypt = $query->getfillableEncrypt();
        }

        $sql = parent::compileSelect($query);

        if ($query->unions) {
            $sql = '(' . $sql . ') ' . $this->compileUnions($query);
        }

        return $sql;
    }

    /**
     * Compile the random statement into SQL.
     */
    #[Override]
    public function compileRandom($seed)
    {
        return 'RAND(' . $seed . ')';
    }

    /**
     * Compile an update statement into SQL.
     */
    #[Override]
    public function compileUpdate(Builder $query, $values)
    {
        $this->columnsEncrypt = [];
        if ($query instanceof BuilderEncrypt) {
            $instance = 'BuilderEncrypt';
            $this->columnsEncrypt = $query->getfillableEncrypt();
        }

        $table = $this->wrapTable($query->from);

        // Each one of the columns in the update statements needs to be wrapped in the
        // keyword identifiers, also a place-holder needs to be created for each of
        // the values in the list of bindings so we can make the sets statements.
        $columns = $this->compileUpdateColumns($values, $this->columnsEncrypt);

        // If the query has any "join" clauses, we will setup the joins on the builder
        // and compile them so we can attach them to this update, as update queries
        // can get join statements to attach to other tables when they're needed.
        $joins = '';

        if (isset($query->joins)) {
            $joins = ' ' . $this->compileJoins($query, $query->joins);
        }

        // Of course, update queries may also be constrained by where clauses so we'll
        // need to compile the where clauses and attach it to the query so only the
        // intended records are updated by the SQL statements we generate to run.
        $where = $this->compileWheres($query);

        $sql = rtrim(sprintf('update %s%s set %s %s', $table, $joins, $columns, $where));

        // If the query has an order by clause we will compile it since MySQL supports
        // order bys on update statements. We'll compile them using the typical way
        // of compiling order bys. Then they will be appended to the SQL queries.
        if (!empty($query->orders)) {
            $sql .= ' ' . $this->compileOrders($query, $query->orders);
        }

        // Updates on MySQL also supports "limits", which allow you to easily update a
        // single record very easily. This is not supported by all database engines
        // so we have customized this update compiler here in order to add it in.
        if (isset($query->limit)) {
            $sql .= ' ' . $this->compileLimit($query, $query->limit);
        }

        return rtrim($sql);
    }

    /**
     * Prepare the bindings for an update statement.
     *
     * Booleans, integers, and doubles are inserted into JSON updates as raw values.
     */
    #[Override]
    public function prepareBindingsForUpdate(array $bindings, array $values)
    {
        $values = collect($values)->reject(fn ($value, $column): bool => $this->isJsonSelector($column) &&
            in_array(gettype($value), ['boolean', 'integer', 'double']))->all();

        return parent::prepareBindingsForUpdate($bindings, $values);
    }

    /**
     * Compile a delete statement into SQL.
     */
    #[Override]
    public function compileDelete(Builder $query)
    {
        $table = $this->wrapTable($query->from);

        $where = is_array($query->wheres) ? $this->compileWheres($query) : '';

        return isset($query->joins)
                    ? $this->compileDeleteWithJoins($query, $table, $where)
                    : $this->compileDeleteWithoutJoins($query, $table, $where);
    }

    /**
     * Compile a single union statement.
     */
    #[Override]
    protected function compileUnion(array $union)
    {
        $conjuction = $union['all'] ? ' union all ' : ' union ';

        return $conjuction . '(' . $union['query']->toSql() . ')';
    }

    /**
     * Compile the lock into SQL.
     */
    #[Override]
    protected function compileLock(Builder $query, $value)
    {
        if (!is_string($value)) {
            return $value ? 'for update' : 'lock in share mode';
        }

        return $value;
    }

    /**
     * Compile all of the columns for an update statement.
     */
    #[Override]
    protected function compileUpdateColumns($values, $columnsEncrypt = [])
    {
        return collect($values)->map(function ($value, $key) use ($columnsEncrypt): string {
            if ($this->isJsonSelector($key)) {
                return $this->compileJsonUpdateColumn($key, new JsonExpression($value));
            }
            $valueParameter = $this->parameter($value);
            if ($key && in_array($key, $columnsEncrypt)) {
                $valueParameter = $this->wrapValueEncrypt($valueParameter);
            }

            return $this->wrap($key) . ' = ' . $valueParameter;

        })->implode(', ');
    }

    /**
     * Prepares a JSON column being updated using the JSON_SET function.
     */
    protected function compileJsonUpdateColumn($key, JsonExpression $value): string
    {
        $path = explode('->', (string) $key);

        $field = $this->wrapValue(array_shift($path));

        $accessor = '"$.' . implode('.', $path) . '"';

        return sprintf('%s = json_set(%s, %s, %s)', $field, $field, $accessor, $value->getValue());
    }

    /**
     * Compile a delete query that does not use joins.
     */
    #[Override]
    protected function compileDeleteWithoutJoins($query, $table, $where)
    {
        $sql = trim(sprintf('delete from %s %s', $table, $where));

        // When using MySQL, delete statements may contain order by statements and limits
        // so we will compile both of those here. Once we have finished compiling this
        // we will return the completed SQL statement so it will be executed for us.
        if (!empty($query->orders)) {
            $sql .= ' ' . $this->compileOrders($query, $query->orders);
        }

        if (isset($query->limit)) {
            $sql .= ' ' . $this->compileLimit($query, $query->limit);
        }

        return $sql;
    }

    /**
     * Compile a delete query that uses joins.
     */
    #[Override]
    protected function compileDeleteWithJoins($query, $table, $where)
    {
        $joins = ' ' . $this->compileJoins($query, $query->joins);

        $alias = str_contains(strtolower($table), ' as ')
                ? explode(' as ', $table)[1] : $table;

        return trim(sprintf('delete %s from %s%s %s', $alias, $table, $joins, $where));
    }

    /**
     * Wrap a single string in keyword identifiers.
     */
    #[Override]
    protected function wrapValue($value, $encrypt = false)
    {
        if ($value === '*') {
            return $value;
        }

        // If the given value is a JSON selector we will wrap it differently than a
        // traditional value. We will need to split this path and wrap each part
        // wrapped, etc. Otherwise, we will simply wrap the value as a string.
        if ($this->isJsonSelector($value)) {
            return $this->wrapJsonSelector($value);
        }

        $value = '`' . str_replace('`', '``', $value) . '`';
        if ($encrypt) {
            $value = $this->wrapValueDecrypt($value);
        }

        return $value;
    }

    /**
     * Wrap a single string in keyword identifiers.
     */
    protected function wrapValueDecrypt($value): string
    {
        return sprintf("AES_DECRYPT(SUBSTRING_INDEX(%s, '.iv.', 1), @AESKEY, SUBSTRING_INDEX(%s, '.iv.', -1))", $value, $value);
    }

    /**
     * Wrap a single string in keyword identifiers.
     */
    protected function wrapValueEncrypt($value): string
    {
        $iv = bin2hex(random_bytes(16));

        return sprintf("CONCAT(AES_ENCRYPT(%s, @AESKEY, '%s'), '.iv.','%s')", $value, $iv, $iv);
    }

    /**
     * Wrap the given JSON selector.
     */
    #[Override]
    protected function wrapJsonSelector($value)
    {
        $path = explode('->', $value);

        $field = $this->wrapValue(array_shift($path));

        return sprintf('%s->\'$.%s\'', $field, collect($path)->map(fn ($part): string => '"' . $part . '"')->implode('.'));
    }

    /**
     * Determine if the given string is a JSON selector.
     */
    #[Override]
    protected function isJsonSelector($value)
    {
        return Str::contains($value, '->');
    }

}
