<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Query\Grammars;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use InvalidArgumentException;
use mrzainulabideen\AESEncrypt\Database\Query\BuilderEncrypt;
use Override;

class PsqlGrammarEncrypt extends PostgresGrammar
{
    protected $AESENCRYPT_KEY;
    protected $AESENCRYPT_MODE;

    protected $columnsEncrypt = [];

    public function __construct()
    {
        $this->AESENCRYPT_KEY = config('aesEncrypt.key');
        $this->AESENCRYPT_MODE = config('aesEncrypt.mode');

        if (empty($this->AESENCRYPT_KEY)) {
            throw new InvalidArgumentException('Set encryption key in .env file, use this alias APP_AESENCRYPT_KEY');
        }

        if (empty($this->AESENCRYPT_MODE)) {
            throw new InvalidArgumentException('Set encryption mode in .env file, use this alias APP_AESENCRYPT_MODE, we recommend using the default: aes-256-cbc');
        }

        $this->AESENCRYPT_MODE = $this->mapChiperAlgo($this->AESENCRYPT_MODE);
    }

    #[Override]
    public function parameterize(array $values, $columnsEncrypt = []): string
    {
        $parameters = [];
        foreach ($values as $key => $value) {
            $valueParameter = $this->parameter($value);
            if ($key && in_array($key, $columnsEncrypt, true)) {
                $valueParameter = $this->wrapValueEncrypt();
            }

            $parameters[] = $valueParameter;
        }

        return implode(', ', $parameters);
    }

    #[Override]
    public function compileInsert(Builder $query, array $values)
    {

        $this->columnsEncrypt = [];
        if ($query instanceof BuilderEncrypt) {
            $this->columnsEncrypt = $query->getfillableEncrypt();
        }

        // Essentially we will force every insert to be treated as a batch insert which
        // simply makes creating the SQL easier for us since we can utilize the same
        // basic routine regardless of an amount of records given to us to insert.
        $table = $this->wrapTable($query->from);

        if (empty($values)) {
            return sprintf('insert into %s default values', $table);
        }

        if (!is_array(reset($values))) {
            $values = [$values];
        }

        $columns = $this->columnize(array_keys(reset($values)));

        // We need to build a list of parameter place-holders of values that are bound
        // to the query. Each insert should have the exact same number of parameter
        // bindings so we will loop through the record and parameterize them all.
        $parameters = collect($values)->map(fn ($record): string => '(' . $this->parameterize($record, $this->columnsEncrypt) . ')')->implode(', ');

        return sprintf('insert into %s (%s) values %s', $table, $columns, $parameters);
    }

    /**
     * Wrap a single string in keyword identifiers.
     */
    private function wrapValueEncrypt(): string
    {
        return sprintf("pgp_sym_encrypt(?, '%s', '%s')", $this->AESENCRYPT_KEY, $this->AESENCRYPT_MODE);
    }

    private function mapChiperAlgo(string $mode): string
    {
        $map = [
            'aes-128-cbc' => 'cipher-algo=aes128',
            'aes-192-cbc' => 'cipher-algo=aes192',
            'aes-256-cbc' => 'cipher-algo=aes256',
        ];

        return $map[$mode] ?? 'cipher-algo=aes256';
    }
}
