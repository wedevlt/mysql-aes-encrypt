<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database;

use Illuminate\Database\PostgresConnection;
use mrzainulabideen\AESEncrypt\Database\Query\Grammars\PsqlGrammarEncrypt as QueryGrammar;
use Override;

class PsqlConnectionEncrypt extends PostgresConnection
{
    /**
     * Get the default query grammar instance.
     */
    #[Override]
    protected function getDefaultQueryGrammar(): QueryGrammar
    {

        return $this->withTablePrefix(new QueryGrammar());
    }
}
