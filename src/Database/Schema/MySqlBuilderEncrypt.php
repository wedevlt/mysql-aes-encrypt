<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database\Schema;

use Illuminate\Database\Schema\MySqlBuilder;
use Override;

class MySqlBuilderEncrypt extends MySqlBuilder
{
    /**
     * Determine if the given table exists.
     */
    #[Override]
    public function hasTable($table)
    {
        $table = $this->connection->getTablePrefix() . $table;

        return count($this->connection->select(
            $this->grammar->compileTableExists(),
            [$this->connection->getDatabaseName(), $table]
        )) > 0;
    }

    /**
     * Get the column listing for a given table.
     */
    #[Override]
    public function getColumnListing($table)
    {
        $table = $this->connection->getTablePrefix() . $table;

        $results = $this->connection->select(
            $this->grammar->compileColumnListing(),
            [$this->connection->getDatabaseName(), $table]
        );

        return $this->connection->getPostProcessor()->processColumnListing($results);
    }
}
