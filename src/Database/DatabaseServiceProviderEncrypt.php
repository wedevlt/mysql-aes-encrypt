<?php

declare(strict_types=1);

namespace mrzainulabideen\AESEncrypt\Database;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\DatabaseServiceProvider;
use mrzainulabideen\AESEncrypt\Database\Connectors\ConnectionFactoryEncrypt;
use Override;

class DatabaseServiceProviderEncrypt extends DatabaseServiceProvider
{
    /**
     * Register the primary database bindings.
     */
    #[Override]
    protected function registerConnectionServices()
    {
        // The connection factory is used to create the actual connection instances on
        // the database. We will inject the factory into the manager so that it may
        // make the connections while they are actually needed and not of before.
        $this->app->singleton('db.factory', fn ($app): ConnectionFactoryEncrypt => new ConnectionFactoryEncrypt($app));

        // The database manager is used to resolve various connections, since multiple
        // connections might be managed. It also implements the connection resolver
        // interface which may be used by other components requiring connections.
        $this->app->singleton('db', fn ($app): DatabaseManager => new DatabaseManager($app, $app['db.factory']));

        $this->app->bind('db.connection', fn ($app) => $app['db']->connection());
    }
}
