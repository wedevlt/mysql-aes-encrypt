<?php

declare(strict_types=1);

namespace Tests\Psql;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Orchestra\Testbench\TestCase;
use Override;
use function Orchestra\Testbench\workbench_path;

class PostgresTestCase extends TestCase
{
    use RefreshDatabase;
    use WithWorkbench;

    #[Override]
    protected function defineEnvironment($app): void
    {
        $app['config']->set('database.default', 'psql');
        $app['config']->set('database.connections.psql', [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', 'pgsql'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'psql_test'),
            'username' => env('DB_USERNAME', 'postgres'),
            'password' => env('DB_PASSWORD', 'secret'),
        ]);
    }

    #[Override]
    protected function defineDatabaseMigrations()
    {
        $this->loadMigrationsFrom(
            workbench_path('database/migrations/mysql')
        );
    }

}
