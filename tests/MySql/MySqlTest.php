<?php

declare(strict_types=1);

namespace Tests\MySql;

use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Attributes\Test;
use Workbench\App\Models\User;

class MySqlTest extends MySqlTestCase
{
    #[Test]
    public function shouldCreateEncryptedName(): void
    {
        $user = User::factory()->create();

        $userDirectly = DB::table('users')->first(['name']);

        $this->assertNotSame($user->name, $userDirectly->name);
    }
}
