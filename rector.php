<?php

declare(strict_types=1);

use Rector\Caching\ValueObject\Storage\FileCacheStorage;
use Rector\CodingStyle\Rector\Stmt\NewlineAfterStatementRector;
use Rector\Config\RectorConfig;
use Rector\DeadCode\Rector\ClassLike\RemoveAnnotationRector;
use Rector\Php55\Rector\String_\StringClassNameToClassConstantRector;
use Rector\Php80\Rector\Catch_\RemoveUnusedVariableInCatchRector;
use Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector;
use Rector\Php80\Rector\Class_\StringableForToStringRector;
use Rector\Php80\Rector\FuncCall\ClassOnObjectRector;
use Rector\Php80\Rector\Identical\StrEndsWithRector;
use Rector\Php80\Rector\Identical\StrStartsWithRector;
use Rector\Php80\Rector\NotIdentical\StrContainsRector;
use Rector\Php80\Rector\Switch_\ChangeSwitchToMatchRector;
use Rector\Php80\Rector\Ternary\GetDebugTypeRector;
use Rector\PHPUnit\Set\PHPUnitSetList;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\TypeDeclaration\Rector\ClassMethod\AddVoidReturnTypeWhereNoReturnRector;
use Rector\TypeDeclaration\Rector\ClassMethod\ReturnTypeFromStrictTypedCallRector;
use Rector\ValueObject\PhpVersion;
use RectorLaravel\Set\LaravelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
        __DIR__ . '/workbench',
        __DIR__ . '/ecs.php',
        __DIR__ . '/rector.php',
    ]);

    $rectorConfig->phpVersion(PhpVersion::PHP_83);
    $rectorConfig->import(LevelSetList::UP_TO_PHP_83);
    $rectorConfig->import(LaravelSetList::LARAVEL_110);
    $rectorConfig->import(SetList::DEAD_CODE);
    $rectorConfig->import(SetList::CODING_STYLE);
    $rectorConfig->import(SetList::TYPE_DECLARATION);
    $rectorConfig->import(PHPUnitSetList::PHPUNIT_110);
    $rectorConfig->import(PHPUnitSetList::PHPUNIT_CODE_QUALITY);
    $rectorConfig->importNames();

    $rectorConfig->rule(StrContainsRector::class);
    $rectorConfig->rule(StrStartsWithRector::class);
    $rectorConfig->rule(StrEndsWithRector::class);
    $rectorConfig->rule(StringableForToStringRector::class);
    $rectorConfig->rule(ClassOnObjectRector::class);
    $rectorConfig->rule(GetDebugTypeRector::class);
    $rectorConfig->rule(RemoveUnusedVariableInCatchRector::class);
    $rectorConfig->rule(ClassPropertyAssignToConstructorPromotionRector::class);
    $rectorConfig->rule(ChangeSwitchToMatchRector::class);
    $rectorConfig->rule(ReturnTypeFromStrictTypedCallRector::class);
    $rectorConfig->rule(AddVoidReturnTypeWhereNoReturnRector::class);
    $rectorConfig->ruleWithConfiguration(RemoveAnnotationRector::class, ['param', 'return']);
    $rectorConfig->cacheClass(FileCacheStorage::class);
    $rectorConfig->cacheDirectory('./storage/framework/cache/rector');

    $rectorConfig->skip([
        NewlineAfterStatementRector::class,
        StringClassNameToClassConstantRector::class => [
            'app/Providers/AppServiceProvider.php',
        ],
    ]);
};
